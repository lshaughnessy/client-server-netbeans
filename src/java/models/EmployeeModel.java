package models;

import dtos.EmployeeDTO;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.sql.DataSource;

/*
 * EmployeeModel.java
 *
 * Created on Aug 31, 2015, 3:03 PM
 *  Purpose:    Contains methods for supporting db access for employee information
 *              Usually consumed by the resoutce class via DTO
 *  Revisions: 
 */
@RequestScoped
public class EmployeeModel implements Serializable {
    public EmployeeModel() {
    }

    public ArrayList<EmployeeDTO> getEmployees(DataSource ds) {
        PreparedStatement pstmt;
        Connection con = null;
        String sql = "SELECT * FROM Employees";
        ArrayList<EmployeeDTO> employees = new ArrayList<>();

        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            try (ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    EmployeeDTO employee = new EmployeeDTO();
                    employee.setTitle(rs.getString("Title"));
                    employee.setEmployeeid(rs.getInt("EmployeeId"));
                    employee.setEmail(rs.getString("Email"));
                    employee.setFirstname(rs.getString("FirstName"));
                    employee.setLastname(rs.getString("LastName"));
                    employee.setPhoneno(rs.getString("Phoneno"));
                    employees.add(employee);
                }
            }
            con.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            System.out.println("SQL issue " + se.getMessage());
        } catch (Exception e) {
            //Handle other errors
            System.out.println("other issue " + e.getMessage());
        } finally {
            //finally block used to close resources
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException se) {
                System.out.println("SQL issue on close " + se.getMessage());
            }//end finally try
        }
        return employees;
    } 
    
    public int addEmployee(EmployeeDTO details, DataSource ds) {
        String sql = "INSERT INTO Employees (Title, FirstName, LastName," + 
                    "Email, Phoneno) VALUES (?,?,?,?,?)";
        Connection con = null;
        PreparedStatement pstmt;
        int employeeid = -1;
        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, details.getTitle());
            pstmt.setString(2, details.getFirstname());
            pstmt.setString(3, details.getLastname());
            pstmt.setString(4, details.getEmail());
            pstmt.setString(5, details.getPhoneno());
            pstmt.execute();
            
            try (ResultSet rs = pstmt.getGeneratedKeys()) {
                rs.next();
                employeeid = rs.getInt(1);
            }//end try
            con.close();
        } catch (SQLException ex) {
           System.out.println("SQL issue on add: " + ex.getMessage());
        }//end try
        return employeeid;
    }//addEmployee
    
    public int updateEmployee(EmployeeDTO details, DataSource ds) {
        String sql = "UPDATE Employees SET Title = ?, FirstName = ?, " +
                "LastName = ?, Phoneno = ? WHERE EmployeeId = ?";
        Connection con = null;
        PreparedStatement pstmt;
        int numOfRows = -1;
        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, details.getTitle());
            pstmt.setString(2, details.getFirstname());
            pstmt.setString(3, details.getLastname());
            pstmt.setString(4, details.getEmail());
            pstmt.setString(5, details.getPhoneno());
            pstmt.setInt(6, details.getEmployeeid());
            numOfRows = pstmt.executeUpdate();
        }
        catch(SQLException ex) {
            System.out.println("SQL issue on update: " + ex.getMessage());
        }
        return numOfRows;
    }//updateEmployee
    public int deleteEmployee(int employeeid, DataSource ds) {
        String sql = "DELETE FROM Employees WHERE employeeid = ?";
        Connection con = null;
        PreparedStatement pstmt;
        int numOfRows = -1;
        try {
            con = ds.getConnection();
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, employeeid);
            numOfRows = pstmt.executeUpdate();
        }
        catch (SQLException ex) {
            System.out.println("SQL issue on delete: " + ex.getMessage());
        }
        return numOfRows;
    }
}