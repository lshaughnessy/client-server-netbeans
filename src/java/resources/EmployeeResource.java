package resources;

import dtos.EmployeeDTO;
import java.net.URI;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import models.EmployeeModel;

/**
 * REST Web Service - Get routine for Vendor
 *
 * @author Evan
 */
@Path("employee")
@RequestScoped
public class EmployeeResource {

    @Context
    private UriInfo context;
    
       // resource already defined in Glassfish
    @Resource(lookup = "jdbc/INFO5059DB")
    DataSource ds;

    /**
     * Creates a new instance of VendorResource
     */
    public EmployeeResource() {
    }

    @GET
    @Produces("application/json")
    public ArrayList<EmployeeDTO> getEmployeesJson() {
        EmployeeModel model = new EmployeeModel();
        ArrayList<EmployeeDTO> employees = model.getEmployees(ds);
        return employees;
    }
    
    /**
     * POST method for creating an instance of Employee
     * @param employee
     * @return an HTTP response with id of created employee
     */
    @POST
    @Consumes("application/json")
    public Response createEmployeeFromJson(EmployeeDTO employee) {
        EmployeeModel model = new EmployeeModel();
        int id = model.addEmployee(employee, ds);
        URI uri = context.getAbsolutePath();
        return Response.created(uri).entity(id).build();
    }
    
    /**
     * PUT method for updating an instance of employee
     * @param employee
     * @return an HTTP response with # of rows updated.
     */
    @PUT
    @Consumes("application/json")
    public Response updateEmployeeFromJson(EmployeeDTO employee) {
        EmployeeModel model = new EmployeeModel();
        int numOfRowsUpdated = model.updateEmployee(employee, ds);
        URI uri = context.getAbsolutePath();
        return Response.created(uri).entity(numOfRowsUpdated).build();
    }
    
    /**
     * DELETE method for removing an instance of Employee
     * @param employeeid
     * @return a number representing the # of rows removed
     */
    @DELETE
    @Path("/{employeeid}")
    public Response deleteEmployee(@PathParam("employeeid")int employeeid) {
        EmployeeModel model = new EmployeeModel();
        int numOfRowsDeleted = model.deleteEmployee(employeeid, ds);
        URI uri = context.getAbsolutePath();
        return Response.created(uri).entity(numOfRowsDeleted).build();
    }
    
}